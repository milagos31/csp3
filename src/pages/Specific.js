import { useState, useEffect, useContext } from "react";
import { Card, Container, Button, InputGroup, FormControl } from "react-bootstrap";
import { Link, useParams, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Specific() {
    const { user } = useContext(UserContext);
    const { productId } = useParams();
    const navigate = useNavigate();

    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [qty, setQty] = useState(1);
    const [price, setPrice] = useState(0);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                setId(data._id);
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            });
    });

    const reduceQty = () => {
        if (qty <= 1) {
            alert("Quantity can't be lower than 1.");
        } else {
            setQty(qty - 1);
        }
    };

    const addToCart = () => {
        let alreadyInCart = false;
        let productIndex;
        let message;
        let cart = [];

        if (localStorage.getItem("cart")) {
            cart = JSON.parse(localStorage.getItem("cart"));
        }

        for (let i = 0; i < cart.length; i++) {
            if (cart[i].productId === id) {
                alreadyInCart = true;
                productIndex = i;
            }
        }

        if (alreadyInCart) {
            cart[productIndex].quantity += qty;
            cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity;
        } else {
            cart.push({
                productId: id,
                name: name,
                price: price,
                quantity: qty,
                subtotal: price * qty,
            });
        }

        localStorage.setItem("cart", JSON.stringify(cart));

        if (qty === 1) {
            message = "1 item added to cart.";
        } else {
            message = `${qty} items added to cart.`;
        }

        alert(message);
    };

    const qtyInput = (value) => {
        if (value === "") {
            value = 1;
        } else if (value === "0") {
            alert("Quantity can't be lower than 1.");
            value = 1;
        }

        setQty(value);
    };

    return (
        <Container>
            <Card className="mt-5 w-75 mx-auto">
                <Card.Header className="bg-secondary text-white text-center pb-0">
                    <h4>{name}</h4>
                </Card.Header>
                <Card.Body>
                    <Card.Text>{description}</Card.Text>
                    <h6>
                        Price: <span className="text-warning">₱{price * qty}</span>
                    </h6>
                    <h6>Quantity:</h6>
                    <InputGroup>
                        <FormControl
                            aria-label="Recipient's username with two button addons"
                            type="number"
                            min="1"
                            value={qty}
                            className="text-center"
                            onChange={(e) => qtyInput(e.target.value)}
                        />
                        <Button variant="outline-secondary" onClick={reduceQty}>
                            ➖
                        </Button>
                        <Button variant="outline-secondary" onClick={() => setQty(qty + 1)}>
                            ➕
                        </Button>
                    </InputGroup>
                </Card.Body>
                <Card.Footer>
                    {user.id !== null ? (
                        user.isAdmin === true ? (
                            <Button variant="danger" block disabled>
                                Admin can't Add to Cart
                            </Button>
                        ) : (
                            <div className="col-md-5 mx-auto">
                                <Button className="m-2" variant="secondary" onClick={() => navigate("/products")}>
                                    Back to Shop
                                </Button>
                                <Button className="m-2" variant="success" onClick={addToCart}>
                                    Save changes
                                </Button>
                            </div>
                        )
                    ) : (
                        <Link
                            className="btn btn-warning btn-block"
                            to={{ pathname: "/login", state: { from: "cart" } }}
                        >
                            Log in to Add to Cart
                        </Link>
                    )}
                    {/* <div className="mt-4">
                        <Link to="/products">Back to shopping.</Link>
                    </div> */}
                </Card.Footer>
            </Card>
        </Container>
    );
}
