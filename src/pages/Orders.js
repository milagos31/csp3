import React, { useState, useEffect } from "react";
import { Container, Card, Accordion } from "react-bootstrap";
import { Link } from "react-router-dom";
import moment from "moment";

export default function Orders() {
    const [ordersList, setOrdersList] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                if (data.length > 0) {
                    let orders = data.reverse().map((item, index) => {
                        return (
                            <Card key={item._id}>
                                <Accordion as={Card.Header} className="bg-secondary text-white">
                                    Order# {item._id}
                                </Accordion>
                                <Card.Body>
                                    <h6>Items:</h6>
                                    <ul>
                                        {item.products.map((subitem) => {
                                            return (
                                                <li key={subitem._id}>
                                                    {subitem.productName} - Quantity: {subitem.quantity}
                                                </li>
                                            );
                                        })}
                                    </ul>
                                    <h6>
                                        Total: <span className="text-warning">₱{item.totalAmount}</span>
                                    </h6>

                                    <h6 className="text-muted">
                                        Purchased on: {moment(item.purchasedOn, true).format("YYYY-MM-DD hh:mm A")}
                                    </h6>
                                </Card.Body>
                            </Card>
                        );
                    });

                    setOrdersList(orders);
                }
            });
    }, []);

    return ordersList.length === 0 ? (
        <div>
            <h3 className="text-center mt-4">
                No orders placed yet! <Link to="/products">Start shopping.</Link>
            </h3>
        </div>
    ) : (
        <Container>
            <h2 className="text-center my-4">Order History</h2>
            <Accordion>{ordersList}</Accordion>
        </Container>
    );
}
