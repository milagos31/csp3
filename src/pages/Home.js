import React from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import { Container } from "react-bootstrap";

export default function Home() {
    const pageData = {
        content: "Wanted items not on Shopee or Lazada? You've come to the right place!",
        destination: "/products",
    };

    return (
        <Container fluid>
            <Banner data={pageData} />
            <h2 className="text-center">Check this out!</h2>
            <Highlights />
        </Container>
    );
}
