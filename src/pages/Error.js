import React from "react";
import { Row, Col, Figure } from "react-bootstrap";
import { Link } from "react-router-dom";
import mylogo from "../g-dinosaur.jpg";

export default function Error() {
    return (
        <Row>
            <Col>
                <div className="text-center">
                    <Figure className="my-3">
                        <h4 id="motto">Page Not Found</h4>
                        <Link className="btn btn-dark" to={"/"}>
                            <Figure.Image src={mylogo} />
                        </Link>
                    </Figure>
                </div>
            </Col>
        </Row>
    );
}
