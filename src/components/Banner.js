import React from "react";
import { Row, Col, Figure } from "react-bootstrap";
import { Link } from "react-router-dom";
import mylogo from "../lashada.png";

export default function Banner({ data }) {
    const { content, destination } = data;
    return (
        <Row>
            <Col>
                <div className="text-center">
                    <Figure className="my-3">
                        <h4 id="motto" className="my-4">
                            {content}
                        </h4>
                        <Link className="btn btn-dark" to={destination}>
                            <Figure.Image alt="La Shada" src={mylogo} />
                        </Link>
                    </Figure>
                </div>
            </Col>
        </Row>
    );
}
