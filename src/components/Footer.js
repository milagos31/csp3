import React from "react";
import { Navbar, Container } from "react-bootstrap";
export default function Footer() {
    return (
        <Navbar fixed="bottom" className="pb-1 footer">
            <Container className="text-center">
                <p className="mx-auto">Copyright © 2022 La Shada Inc. - All Rights Reserved</p>
            </Container>
        </Navbar>
    );
}
