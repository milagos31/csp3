import React from "react";
import { Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Product(props) {
    const { breakPoint, data } = props;

    const { _id, name, description, price } = data;

    return (
        <Col sm={12} md={6} lg={4} className="mb-4">
            <Card className="card1" style={{ width: "18rem" }}>
                <Card.Body className="bg-light">
                    <Card.Title className="text-center card2">
                        <Link to={`/products/${_id}`}>{name}</Link>
                    </Card.Title>
                    <Card.Text className="card3">{description}</Card.Text>
                    <h5 className="text-warning">₱{price}</h5>
                </Card.Body>
                <Card.Footer className="text-center">
                    <Link className="btn btn-secondary btn-block" to={`/products/${_id}`}>
                        Details
                    </Link>
                </Card.Footer>
            </Card>
        </Col>
    );
}
